package soft.eng.project.Provider;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProviderRepository extends JpaRepository<Provider,Long>{

	Provider findByEmail(String email);

}
