package soft.eng.project.Provider;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ProviderController {
	
	@Autowired
	private ProviderService providerService;

	
	@PostMapping(path="/registerProvider")
	public  @ResponseBody String registerProvider (@RequestBody Provider provider) {
		return providerService.registerProvider(provider);
	}
	// @ResponseBody means the returned String is the response, not a view name
	// @RequestParam means it is a parameter from the GET or POST request
	
	@PostMapping(path="/loginProvider")
	public  @ResponseBody Provider loginProvider (@RequestBody Provider provider) {
		return providerService.loginProvider(provider);
	}

}
