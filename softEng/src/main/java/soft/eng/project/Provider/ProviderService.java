package soft.eng.project.Provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProviderService {


	@Autowired
	private ProviderRepository providerRepository;
	
	public Provider loginProvider(Provider provider) {
		Provider p = new Provider();
		p = providerRepository.findByEmail(provider.getEmail());
		
		if(p!=null) {
				System.out.println("Found some user");
				if (p.getPassword().equals(provider.getPassword())) {
					return p;
				}
				else {
					System.out.println("Wrong Password");
					return null;
				}
			}
			else {
		
				System.out.println("user not in the database");
					return null;
				}
		}

	public String registerProvider(Provider provider) {
			System.out.println(provider.getFirstName());
			providerRepository.save(provider);
			return "Saved";
		}
}