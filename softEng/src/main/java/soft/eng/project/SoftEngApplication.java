package soft.eng.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoftEngApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoftEngApplication.class, args);
	}
}
