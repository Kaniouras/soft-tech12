package soft.eng.project.Event;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Event {
	
/*	public Event() {
		
	}
	
	public Event(String email, int tickets, int price, String address, int minAge, String category, Date kickoffDate,
			String description) {
		super();
		this.email = email;
		this.tickets = tickets;
		this.ticketsLeft = tickets;
		this.price = price;
		this.address = address;
		this.minAge = minAge;
		this.category = category;
		this.kickoffDate = kickoffDate;
		this.description = description;
	}
	*/
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int Id;
	private String email;
	private int tickets;
	private int price;
	private String address;
	private int minAge;
	private String category;
	private int ticketsLeft;
	private Date releaseDate;
	private Date kickoffDate;
	private String description;

	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getTickets() {
		return tickets;
	}
	public void setTickets(int tickets) {
		this.tickets = tickets;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getMinAge() {
		return minAge;
	}
	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public int getTicketsLeft() {
		return ticketsLeft;
	}
	public void setTicketsLeft(int ticketsLeft) {
		this.ticketsLeft = ticketsLeft;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	public Date getKickoffDate() {
		return kickoffDate;
	}
	public void setKickoffDate(Date kickoffDate) {
		this.kickoffDate = kickoffDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
