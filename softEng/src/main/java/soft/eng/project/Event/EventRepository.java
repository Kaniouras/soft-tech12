package soft.eng.project.Event;

//import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EventRepository extends JpaRepository<Event,Long>{
	List<Event> findByMinAgeGreaterThanEqual(int age);

	List<Event> findByCategory(String category);

	List<Event> findByPriceGreaterThanEqual(int price);

}
