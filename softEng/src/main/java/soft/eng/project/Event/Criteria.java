package soft.eng.project.Event;

import java.util.Date;

public class Criteria {
		String Category;
		int MinAge;
		int price;
		Date KickOffDate;
		String Description;
		
		public String getCategory() {
			return Category;
		}
		public void setCategory(String category) {
			Category = category;
		}
		public int getMinAge() {
			return MinAge;
		}
		public void setMinAge(int minAge) {
			MinAge = minAge;
		}
		public int getPrice() {
			return price;
		}
		public void setPrice(int price) {
			this.price = price;
		}
		public Date getKickOffDate() {
			return KickOffDate;
		}
		public void setKickOffDate(Date kickOffDate) {
			KickOffDate = kickOffDate;
		}
		public String getDescription() {
			return Description;
		}
		public void setDescription(String description) {
			Description = description;
		}
	}

