package soft.eng.project.Event;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class EventController {
	//@Autowired
//	private EventRepository eventRepository;
	@Autowired
	private EventService eventService;

	@RequestMapping(path="/addEvent",method = RequestMethod.POST)
	public  @ResponseBody String addEvent (@RequestBody Event event) {
		return eventService.addEvent(event);
	}
	
	@RequestMapping(path="/searchEvents", method=RequestMethod.POST)
	public @ResponseBody Set<Event> searchEvents(@RequestBody Criteria crit){
		return eventService.searchEvents(crit);	
	}
}
