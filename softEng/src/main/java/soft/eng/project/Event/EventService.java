package soft.eng.project.Event;

import java.util.HashSet;
import java.util.Set;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventService {
	
	@Autowired
	private EventRepository eventRepository;
	

	public Set<Event> searchEvents(Criteria crit) {
		//search by age, price , type . others are google maps and free text
		Set<Event> Events = new HashSet<Event>();
		//by price
		Events.addAll(eventRepository.findByPriceGreaterThanEqual(crit.getPrice()));
		//by age
		Events.addAll(eventRepository.findByMinAgeGreaterThanEqual(crit.getMinAge()));
		//by type/category
		Events.addAll(eventRepository.findByCategory(crit.getCategory()));
		return Events;
	}

	public String addEvent(Event event) {
		System.out.println(event);
		eventRepository.save(event);
		return "added successfully";
	}

}
