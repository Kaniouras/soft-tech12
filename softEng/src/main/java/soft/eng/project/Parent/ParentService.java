package soft.eng.project.Parent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import org.springframework.web.bind.annotation.RequestBody;

@Service
public class ParentService {
	
		@Autowired
		private ParentRepository parentRepository;
		
		public Parent loginParent (Parent parent) {
			if(parentRepository.findByEmail(parent.getEmail())!=null) {
				System.out.println("Found some user");
				Parent p = new Parent();
				p = parentRepository.findByEmail(parent.getEmail());
				if (p.getPassword().equals(parent.getPassword())) {
					return p;
				}
				else {
					System.out.println("Wrong Password");
					return null;
				}
			}
			else {
		
				System.out.println("user not in the database");
					return null;
				}
		}

		public String registerParent(Parent parent) {
			System.out.println(parent.getFirstName());
			parentRepository.save(parent);
			return "Saved";
		}
}