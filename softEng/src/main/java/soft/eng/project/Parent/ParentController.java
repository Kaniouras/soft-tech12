package soft.eng.project.Parent;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path="/demo",method=RequestMethod.POST) // This means URL's start with /demo (after Application path)
public class ParentController {
	
//	@Autowired
//	private ParentRepository parentRepository;

	@Autowired
	private ParentService parentService;


	@GetMapping(path="/registerParent")
	public  @ResponseBody String registerParent (@RequestBody Parent parent) {
		return parentService.registerParent(parent);
	}
	// @ResponseBody means the returned String is the response, not a view name
	// @RequestParam means it is a parameter from the GET or POST request
	
	@GetMapping(path="/login")
	public  @ResponseBody Parent loginParent (@RequestBody Parent parent) {
		return parentService.loginParent(parent);
	}
	
	@GetMapping(path="/searchEvents")
	public String searchEvents(@RequestBody soft.eng.project.Event.Criteria crit){
		//need a way to get the criteria
		//solve the redirect thing
		//make sure that paths work properly ->
		//->going from ParentController url to eventController url
		return "redirect:toThePasthForTheEventControllerAndThatMethod";
	}
}
