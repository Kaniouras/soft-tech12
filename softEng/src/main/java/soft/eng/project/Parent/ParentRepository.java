package soft.eng.project.Parent;

import org.springframework.data.repository.CrudRepository;

import soft.eng.project.Parent.Parent;

public interface ParentRepository extends CrudRepository<Parent,Long>{
	Parent findByEmail(String email);
}
